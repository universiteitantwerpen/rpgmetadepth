"Starting simulation...".println();
var currentState : State := State.allInstances().select(i | i.initial).first();
var currentInterruptTransition : InterruptTransition;
var currentTimedTransition : TimedTransition;
var currentInterrupt : Interrupt := Interrupt.allInstances().select(i | i.prev.isUndefined()).first();
var time : Time := Time.allInstances().first();

while (not stopCondition()) {
	("Current state: " + currentState.name + " at time " + time.clock).println();
	if (not hasOutTimedTransition() or
		(currentInterrupt.at <= time.clock + currentTimedTransition.after
		 and hasOutInterruptTransition())) {
		// the interrupt will be processed
		currentState = interruptTransition();
	} else if (hasOutTimedTransition()) {
		// go for the timed transition
		currentState = timedTransition();
	} else {
		break;
	}
}

("Stopped simulation at time " + time.clock).println();

operation stopCondition() : Boolean {
	return currentState.out_states_timed.isUndefined()
		   and (currentState.out_states_interrupt.size() = 0
	            or Interrupt.allInstances.excludes(currentInterrupt)); // hack for currentInterrupt = null which does not work due to a bug
}

operation hasOutInterruptTransition() : Boolean {
	var interrupt_transitions = InterruptTransition.allInstances.select(i | i.out_states_interrupt = currentState and i.interrupt = currentInterrupt.event);
	if (interrupt_transitions.size() > 0) {
		currentInterruptTransition = interrupt_transitions.first();
		return true;
	}
	return false;
}

operation interruptTransition() : State {
	("Interrupt " + currentInterrupt.event + " at " + currentInterrupt.at + " ms").println();
	time.clock = currentInterrupt.at;
	if (currentInterrupt.next.isDefined()) {
		currentInterrupt = currentInterrupt.next;
	} else {
		currentInterrupt = null;
	}
	return currentInterruptTransition.in_states;
}

operation hasOutTimedTransition() : Boolean {
	var timed_transitions = TimedTransition.allInstances().select(i | i.out_states_timed = currentState);
	if (timed_transitions.size() > 0) {
		currentTimedTransition = timed_transitions.first();
		return true;
	}
	return false;
}

operation timedTransition() : State {
	time.clock = time.clock + currentTimedTransition.after;
	return currentTimedTransition.in_states;
}

