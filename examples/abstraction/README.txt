This .zip file contains a set of reusable abstractions and some test meta-models and models (in the "metamodels" folder). In order to use these abstractions, 
just start metaDepth with:

java -jar metaDepth.jar

Now for example you can use the Workflow abstractions, over the Intalio's BPMN meta-model, by typing the following commands in the console:

set DIR "abstraction/"
load "metamodels/BPMN/bpmnWorkflow"
context TestSequence
load EOL "Workflow/abstract.eol"

Now some options should appear, you may press 1 for sequential abstractions, and now 3 for exit. Then, you can type:

dump

to see the abstracted model, with some created SubProcesses. Workflow abstractions can also be used with other meta-models, like ActivityDiagrams or the OMNG's BPMN 2.
You might also use these abstractions with a DSL of your own. For example, you might type:

load "metamodels/Factory/factoryProcessHolder"
context aFactory
load EOL "ProcessHolder/abstract.eol"

To abstract a DSL for factories using abstractions for Petri nets. 

Additionally, the folders include some reusable clustering algorithms (using Formal Concept Analysis and K-Means), which can be used for similarity-based abstraction. 
For example, type:

set DIR "abstraction/fca/"
load "BPMN"
context pc2
load EOL "abstractFCA.eol"

to abstract a BPMN using FCA and the resources used by tasks as similarity criteria.

For questions, e-mail Juan.deLara@uam.es