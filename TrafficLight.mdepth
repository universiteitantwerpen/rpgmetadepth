Model TrafficLight {
	Node Visualisation {
		red : boolean = false;
		green : boolean = false;
		yellow : boolean = false;
		states : State[*] {ordered,unique};
	}

	Node State {
		name : String {id};
		initial : boolean = false;
		out_states_timed : State[0..1] {ordered,unique};
		out_states_interrupt : State[*] {ordered,unique};
		in_states : State[*] {ordered,unique};
		visualisation : Visualisation[1] {ordered,unique};
	}
	oneInitial:$State.allInstances().one(s | s.initial)$

	Edge TimedTransition(State.out_states_timed, State.in_states) {
		after : int;
	}

	Edge InterruptTransition(State.out_states_interrupt, State.in_states) {
		interrupt : String;
	}
	
	Edge VisualizedBy(State.visualisation, Visualisation.states) {}

	Node Time[1] {
		clock : int = 0;
	}

	Node Interrupt {
		at : int;
		event : String;
		next : Interrupt[0..1] {ordered,unique};
		prev : Interrupt[0..1] {ordered,unique};
	}
	
	Edge Next(Interrupt.next, Interrupt.prev) {}
}

TrafficLight myTrafficLight {
	// green - yellow - red traffic light, with the
	// possibility for a policeman to "interrupt" this
	// regular behaviour so that the light starts blinking yellow
	Visualisation redVis {
		red = true;
	}
	Visualisation greenVis {
		green = true;
	}
	Visualisation yellowVis {
		yellow = true;
	}
	Visualisation blankVis {}

	// standard routine
	State red {
		initial = true;
		name = "red";
	}
	State green {
		initial = true;
		name = "green";
	}
	State yellow {
		name = "yellow";
	}
	VisualizedBy(red, redVis) {}
	VisualizedBy(green, greenVis) {}
	VisualizedBy(yellow, yellowVis) {}
	TimedTransition toGreen(red, green) {
		after = 10000;
	}
	TimedTransition toYellow(green, yellow) {
		after = 6000;
	}
	TimedTransition toRed(yellow, red) {
		after = 2000;
	}

	// blinking routine
	State blink {
		name = "blink";
	}
	State unblink {
		name = "unblink";
	}
	VisualizedBy(blink.visualisation, yellowVis.states) {}
	VisualizedBy(unblink.visualisation, blankVis.states) {}
	TimedTransition toBlink(unblink, blink) {
		after = 1000;
	}
	TimedTransition toUnblink(blink, unblink) {
		after = 1000;
	}

	// transitions between routines
	InterruptTransition Police1(red, blink) {
		interrupt = "Policeman";
	}
	InterruptTransition Police2(green, blink) {
		interrupt = "Policeman";
	}
	InterruptTransition Police3(yellow, blink) {
		interrupt = "Policeman";
	}
	InterruptTransition Light1(blink, red) {
		interrupt = "Light";
	}
	InterruptTransition Light2(unblink, red) {
		interrupt = "Light";
	}

	// terminate
	State terminated {
		name = "terminated";
	}
	VisualizedBy(terminated.visualisation, blankVis.states) {}
	InterruptTransition Terminate1(red, terminated) {
		interrupt = "Stop";
	}
	InterruptTransition Terminate2(green, terminated) {
		interrupt = "Stop";
	}
	InterruptTransition Terminate3(yellow, terminated) {
		interrupt = "Stop";
	}
	InterruptTransition Terminate4(blink, terminated) {
		interrupt = "Stop";
	}
	InterruptTransition Terminate5(unblink, terminated) {
		interrupt = "Stop";
	}

	// environment
	Time t {}
	Interrupt i1 {
		at = 40000;
		event = "Policeman";
	}
	Interrupt i2 {
		at = 60000;
		event = "Light";
	}
	Interrupt i3 {
		at = 90000;
		event = "Stop";
	}
	Next(i1, i2);
	Next(i2, i3);
}